const router = require('express').Router();

const RecipeController = require('../controllers/RecipeController');

router.get('/', (req, res) => {
   res.status(200).send('Gousto Test API.');
});

router.get('/recipes/list', RecipeController.list);
router.get('/recipes/:recipeId', RecipeController.show);
router.get('/recipes/cuisine/:cuisine/page/:pageNumber', RecipeController.listByCuisine);
router.patch('/recipes/:recipeId', RecipeController.update);
router.post('/recipes', RecipeController.create);
router.post('/recipe/:recipeId/rate', RecipeController.rate);
router.get('/recipe/:recipeId/ratings', RecipeController.listRatings);

module.exports = router;