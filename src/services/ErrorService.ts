
export interface IGoustoError {
    message: string;
    status: number;
}

export default class GoustoError extends Error implements IGoustoError {

    public status: number = null;

    constructor(message, status = 500) {
        super(message);
        this.status = status;
    }

    get message() {
        return super.message;
    }

}
