const sqlite3Db = require("sqlite3").verbose();
import sqlite3 from "sqlite3";
import GoustoError from "./ErrorService";

export interface IDatabaseService {
    db: sqlite3.Database;
    getConnection(): sqlite3.Database;
}

export default class DatabaseService implements IDatabaseService {

    public file: string = "./data/gousto.sqlite";

    public db: sqlite3.Database = null;

    public getConnection(): sqlite3.Database {
        if (this.db) {
            return this.db;
        }
        this.db = new sqlite3Db.Database(this.file, (err) => {
            if (err) {
                throw new GoustoError(err);
            }
        });
        return this.db;
    }

}
