import Recipe from '../models/Recipe';
import { IPaginatedResponse, IRating, IRecipe, IRecipeList } from "../interfaces/recipes";
import GoustoError from "./ErrorService";

export interface IRecipeService {
    db: any,
    list(): Promise<IRecipeList>,
    getById(recipeId: number): Promise<IRecipe>,
    getByCuisine(cuisine: string, page: number): Promise<IRecipeList>,
    update(recipeId: number, model: IRecipe): Promise<IRecipe>,
    create(model: Recipe): Promise<IRecipe>,
    rateRecipe(recipeId: number, rating: number): Promise<{}>,
    listRatings(recipeId: number): Promise<IRating[]>
}

export default class RecipeService implements IRecipeService {

    db: any = null;

    constructor(database: any) {
        this.db = database;
    }

    async list(): Promise<IRecipeList> {

        return new Promise<IRecipeList>((resolve, reject) => {

            const sql: string = 'SELECT * FROM recipes;';

            this.db.all(sql, (err, rows) => {
                if (err) {
                    return reject(err);
                }
                resolve(rows);
            });

        });

    }

    async getById(recipeId: number): Promise<IRecipe> {

        return new Promise<IRecipe>((resolve, reject) => {

            if (!recipeId) {
                return reject(new GoustoError('You must provide a Recipe ID.', 400));
            }

            const sql: string = 'SELECT * FROM recipes WHERE id = ?';

            this.db.get(sql, [recipeId], (err, row) => {
                if (err) {
                    return reject(err);
                }
                resolve(row);
            });

        });

    }

    async getByCuisine(cuisine: string, page: number = 1): Promise<IRecipeList> {

        const resultsPerPage = 2;

        page -= 1; //front end doesn't want to be 0 based

        return new Promise<IRecipeList>((resolve, reject) => {

            if (!cuisine) {
                return reject(new GoustoError('Missing cuisine parameter.', 400));
            }

            const sql: string = 'SELECT * FROM recipes WHERE recipe_cuisine = ?';

            this.db.all(sql, [cuisine], (err, rows) => {

                if (err) {
                    return reject(err);
                }

                const pageCount: number = Math.round(rows.length / resultsPerPage );
                const skip: number = resultsPerPage * page;
                let recipes: IRecipe[] = [];

                let skipped: number = 0;
                let count: number = 0;

                for (let row of rows) {
                    if (count === resultsPerPage) {
                        break;
                    }
                    if (skip && skipped < skip) {
                        skipped++;
                        continue;
                    }
                    recipes.push(row);
                    count++;
                }

                const response: IPaginatedResponse = {
                    currentPage: page + 1,
                    pageCount: pageCount,
                    size: recipes.length,
                    recipes: recipes
                };

                resolve(response);

            });

        });

    }

    async update(recipeId: number, model: IRecipe): Promise<IRecipe> {

        return new Promise<IRecipe>((resolve, reject) => {

            if (!recipeId) {
                return reject(new GoustoError('You must pass a recipeID.', 400));
            }

            let sql: string = 'UPDATE recipes';

            let updateCount: number = 0;

            let values: any[] = [];

            const Model = new Recipe();

            for (let property in model) {

                if (!Model.hasOwnProperty(property)) {
                    return reject(new GoustoError(`${property} is not a known property of Recipe.`, 400));
                }

                sql += `${(updateCount > 0) ? ',' : ''} SET ${property} = ? `;

                values.push(model[property]);

                updateCount++;

            }

            sql += ` WHERE id = ? `;

            values.push(recipeId);

            this.db.run(sql, values, (err) => {
                if (err) {
                    return reject(err);
                }
                this.db.get(`SELECT * FROM recipes WHERE id = ${recipeId}`, (err, row) => {
                   if (err) {
                       return reject(err);
                   }
                   resolve(row);
                });
            });

        });

    }

    async create(model: Recipe): Promise<IRecipe>  {

        return new Promise<IRecipe>((resolve, reject) => {

            const RecipeModel = new Recipe();

            let values: any[] = [];

            let sql: string = 'INSERT INTO recipes (';

            let insertCount: number = 0;

            for (let property in model) {
                if (!RecipeModel.hasOwnProperty(property)) {
                    return reject(new GoustoError(`${property} is not a known property of Recipe.`, 400));
                }
                values.push(model[property]);
                sql += `${(insertCount > 0) ? ',' : ''}${property}`;
                insertCount++;
            }

            sql += ') VALUES (';

            let valueCount: number = 0;

            for (let i in values) {
                sql += `${(valueCount > 0) ? ',' : ''}?`;
                valueCount++;
            }

            sql += ')';

            let self = this;

            this.db.run(sql, values, function (err) {
                if (err) {
                    return reject(err);
                }
                self.db.get(`SELECT * FROM recipes WHERE id = ${this.lastID}`, (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

        });

    }

    async rateRecipe(recipeId: number, rating: number): Promise<{}> {

        return new Promise<{}>((resolve, reject) => {

            if (!recipeId) {
                return reject(new GoustoError('You must provide a Recipe ID.', 400));
            }

            const sql: string = 'INSERT INTO ratings (recipe_id, rating, created_at) VALUES (?, ?, CURRENT_TIMESTAMP)';

            const values = [recipeId, rating];

            this.db.run(sql, values, err => {
                if (err) {
                    return reject(err);
                }
                resolve();
            });

        });

    }

    async listRatings(recipeId: number): Promise<IRating[]> {

        return new Promise<IRating[]>((resolve, reject) => {

            const sql: string = 'SELECT * FROM ratings WHERE recipe_id = ?';

            this.db.all(sql, [recipeId], (err, rows) => {
                if (err) {
                    return reject(err);
                }
                resolve(rows);
            });

        });

    }

}