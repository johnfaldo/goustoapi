
export interface IBaseRecipe {
    created_at: string;
    box_type: string;
    title: string;
    slug: string;
    marketing_description: string;
    calories_kcal: number;
    protein_grams: number;
    fat_grams: number;
    carbs_grams: number;
    recipe_diet_type_id: string;
    season: string;
    protein_source: string;
    preparation_time_minutes: number;
    shelf_life_days: number;
    equipment_needed: string;
    origin_country: string;
    recipe_cuisine: string;
    gousto_reference: number;
}

export interface IRecipe extends IBaseRecipe {
    id: number;
    updated_at: string;
    short_title: string;
    bulletpoint1: string;
    bulletpoint2: string;
    bulletpoint3: string;
    base: string;
    in_your_box: string;
}

export interface IRecipeList {
    recipes: IRecipe[];
}

export interface IRecipeResponse {
    recipe: IRecipe;
}

export interface IRecipeListResponse {
    recipes: IRecipeList;
}

export interface IPaginatedResponse {
    recipes: IRecipe[];
    size: number;
    currentPage: number;
    pageCount: number;
}

export interface IRatingResponse {
    message: string;
}

export interface IRating {
    id: number;
    recipe_id: number;
    rating: number;
    created_at: string;
}

export interface IListRatingsResponse {
    ratings: IRating[];
}
