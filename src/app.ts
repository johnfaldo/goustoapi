import bodyParser from 'body-parser';
import session from 'express-session';
import cors from 'cors';
import errorhandler from 'errorhandler';
import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import methodOverride from 'method-override';
import GoustoError from "./services/ErrorService";

dotenv.config();

const isProduction: boolean = ("production" === process.env.NODE_ENV);

const app: express.Application = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(session({ secret: "conduit", cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  }));
app.use(morgan("combined", { skip: (req, res) => (res.statusCode < 400) }));

if (!isProduction) {
    app.use(errorhandler());
}

app.use(require("./routes"));

app.use((req, res, next) => {
    next(new GoustoError("Not Found", 404));
});

// error handler
app.use((err, req, res, next) => {
    res.status(err.status || 500).json({errors: {message: err.message}});
});

module.exports = app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`);
});