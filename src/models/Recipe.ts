import { IRecipe } from "../interfaces/recipes";
import BaseRecipe from "./BaseRecipe";

export default class Recipe extends BaseRecipe implements IRecipe {

    public id: number;
    public updated_at: string;
    public short_title: string;
    public bulletpoint1: string;
    public bulletpoint2: string;
    public bulletpoint3: string;
    public base: string;
    public in_your_box: string;

    constructor() {
        super();
        this.id = null;
        this.updated_at = null;
        this.short_title = null;
        this.bulletpoint1 = null;
        this.bulletpoint2 = null;
        this.bulletpoint3 = null;
        this.base = null;
        this.in_your_box = null;
    }

}
