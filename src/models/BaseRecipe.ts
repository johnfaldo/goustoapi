import { IBaseRecipe } from "../interfaces/recipes";

export default class BaseRecipe implements IBaseRecipe {

    public created_at: string;
    public box_type: string;
    public title: string;
    public slug: string;
    public marketing_description: string;
    public calories_kcal: number;
    public protein_grams: number;
    public fat_grams: number;
    public carbs_grams: number;
    public recipe_diet_type_id: string;
    public season: string;
    public protein_source: string;
    public preparation_time_minutes: number;
    public shelf_life_days: number;
    public equipment_needed: string;
    public origin_country: string;
    public recipe_cuisine: string;
    public gousto_reference: number;

    constructor() {
        this.created_at = null;
        this.box_type = null;
        this.title = null;
        this.slug = null;
        this.marketing_description = null;
        this.calories_kcal = null;
        this.protein_grams = null;
        this.fat_grams = null;
        this.carbs_grams = null;
        this.recipe_diet_type_id = null;
        this.season = null;
        this.protein_source = null;
        this.preparation_time_minutes = null;
        this.shelf_life_days = null;
        this.equipment_needed = null;
        this.origin_country = null;
        this.recipe_cuisine = null;
        this.gousto_reference = null;
    }

}
