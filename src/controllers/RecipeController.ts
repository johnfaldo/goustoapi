import {
    IPaginatedResponse,
    IRatingResponse,
    IRecipe,
    IRecipeList,
    IRecipeListResponse,
    IRecipeResponse,
    IListRatingsResponse, IRating
} from "../interfaces/recipes";
import DatabaseService from '../services/DatabaseService';
import RecipeService, { IRecipeService } from '../services/RecipeService';
import BaseRecipe from '../models/BaseRecipe';
import GoustoError from '../services/ErrorService';

let recipeService: IRecipeService = new RecipeService(new DatabaseService().getConnection());

exports.list = async (req, res, next) => {

    let error = false;

    const recipes: IRecipeList = await recipeService.list().catch(err => {
        error = true;
        return next(err);
    });

    if (error) {
        return;
    }

    const response: IRecipeListResponse = {recipes};

    res.status(200).json(response);

};

exports.show = async (req, res, next) => {

    let error = false;

    const recipe: IRecipe = await recipeService.getById(req.params.recipeId).catch(err => {
        error = true;
        return next(err);
    });

    if (error) {
        return;
    }

    const response: IRecipeResponse = {recipe};

    res.status(200).json(response);

};

exports.listByCuisine = async (req, res, next) => {

    let error = false;

    const response: IPaginatedResponse = await recipeService.getByCuisine(
        req.params.cuisine,
        req.params.pageNumber
    ).catch(err => {
        error = true;
        return next(err);
    });

    if (error) {
        return;
    }

    res.status(200).json(response);

};

exports.update = async (req, res, next) => {

    let error = false;

    let propertyCount = 0;
    for (let i in req.body) {
        propertyCount++;
    }

    if (!propertyCount) {
        return next(new GoustoError('Empty request body.', 400));
    }

    const recipe: IRecipe = await recipeService.update(req.params.recipeId, req.body).catch(err => {
        error = true;
        return next(err);
    });

    if (error) {
        return;
    }

    const response: IRecipeResponse = {recipe};

    res.status(200).json(response);

};

exports.create = async (req, res, next) => {

    let error = false;

    let MinimumModel: BaseRecipe = new BaseRecipe();

    for (let property in MinimumModel) {
        if (!req.body.hasOwnProperty(property)) {
            return next(new GoustoError(`${property} is a required property of Recipe.`));
        }
    }

    const recipe: IRecipe = await recipeService.create(req.body).catch(err => {
        error = true;
        return next(err);
    });

    if (error) {
        return;
    }

    const response: IRecipeResponse = {recipe};

    res.status(200).json(response);

};

exports.rate = async (req, res, next) => {

    let error = false;

    if (!req.body.rating || !(req.body.rating > 0 && req.body.rating < 6)) {
        return next(new GoustoError('rating field is required, and must be between 1 and 5.'), 400);
    }

    await recipeService.rateRecipe(req.params.recipeId, req.body.rating).catch(err => {
        error = true;
        return next(err);
    });

    if (error) {
        return;
    }

    const response: IRatingResponse = {
        message: 'Rating submitted.'
    };

    res.status(200).json(response);

};

exports.listRatings = async (req, res, next) => {

    let error = false;

    const ratings: Array<IRating> = await recipeService.listRatings(req.params.recipeId).catch(err => {
        error = true;
        return next(err);
    });

    if (error) {
        return;
    }

    const response: IListRatingsResponse = {ratings};

    res.status(200).json(response);

};
