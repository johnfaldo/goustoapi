const fs = require('fs');
const parse = require('csv-parse/lib/sync');

const DatabaseService = require('./connection');

const db = DatabaseService.getConnection();

const recipeTableCreate = () => {

    return new Promise(async (resolve, reject) => {

        const sql = `CREATE TABLE recipes (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          created_at DATETIME,
          updated_at DATETIME,
          box_type TEXT DEFAULT '',
          title TEXT DEFAULT '',
          slug TEXT DEFAULT '',
          short_title TEXT DEFAULT '',
          marketing_description TEXT DEFAULT '', 
          calories_kcal INTEGER,
          protein_grams INTEGER,
          fat_grams INTEGER,
          carbs_grams INTEGER,
          bulletpoint1 TEXT DEFAULT '',
          bulletpoint2 TEXT DEFAULT '',
          bulletpoint3 TEXT DEFAULT '',
          recipe_diet_type_id TEXT DEFAULT '',
          season TEXT DEFAULT '',
          base TEXT DEFAULT '',
          protein_source TEXT DEFAULT '',
          preparation_time_minutes INTEGER,
          shelf_life_days INTEGER,
          equipment_needed TEXT DEFAULT '',
          origin_country TEXT DEFAULT '',
          recipe_cuisine TEXT DEFAULT '',
          in_your_box TEXT DEFAULT '',
          gousto_reference INTEGER
        )`;

        db.run(sql, (err) => {
            if (err) {
                return reject(err);
            }
            resolve();
        });

    });

};

const importRecipesFromCsv = () => {

    return new Promise(async (resolve, reject) => {

        const input = fs.readFileSync('./data/recipe-data.csv', 'utf8');

        const records = parse(input, {
            columns: true,
            skip_empty_lines: true
        });

        const count = records.length;

        let i = 0;

        for (let row of records) {

            const sql = `INSERT INTO recipes (
              id,
              created_at,
              updated_at,
              box_type,
              title,
              slug,
              short_title,
              marketing_description, 
              calories_kcal,
              protein_grams,
              fat_grams,
              carbs_grams,
              bulletpoint1,
              bulletpoint2,
              bulletpoint3,
              recipe_diet_type_id,
              season,
              base,
              protein_source,
              preparation_time_minutes,
              shelf_life_days,
              equipment_needed,
              origin_country,
              recipe_cuisine,
              in_your_box,
              gousto_reference
            ) VALUES (
              ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
            )`;

            let values = [];

            for (let column in row) {
                values.push(row[column]);
            }

            db.run(sql, values, (err) => {
                if (err) {
                    return reject(err);
                }

                i++;

                if (i === count) {
                    resolve(records);
                }

            });

        }

    });

}

const ratingTableCreate = () => {

    return new Promise(async (resolve, reject) => {

        const sql = `CREATE TABLE ratings (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          recipe_id INTEGER,
          rating INTEGER,
          created_at DATETIME
        )`;

        db.run(sql, (err) => {
            if (err) {
                return reject(err);
            }
            resolve();
        });

    });

};

module.exports = {
    recipeTableCreate: recipeTableCreate,
    importRecipesFromCsv: importRecipesFromCsv,
    ratingTableCreate: ratingTableCreate
};

