const sqlite3Db = require('sqlite3').verbose();

class DatabaseService {

    constructor() {
        this.db = null;
    }

    getConnection() {
        if (this.db) {
            return this.db;
        }
        this.db = new sqlite3Db.Database('./data/gousto.sqlite', (err) => {
            if (err) {
                throw new Error(err);
            }
        });
        return this.db;
    }

}

module.exports = new DatabaseService();