const migrate = require('./data/migration');

(async () => {

    let error = false;

    await migrate.recipeTableCreate().catch(err => {
        if (err) {
            console.error(err);
            console.error('Migration stopped at recipe table create.');
            error = true;
        }
    });

    if (error) {
        return;
    }

    console.log('Recipe table created.');

    const records = await migrate.importRecipesFromCsv().catch(err => {
       console.error(err);
        console.error('Migration stopped at import recipes from CSV.');
        error = true;
    });

    if (error) {
        return;
    }

    console.log(`${records.length} records imported.`);

    await migrate.ratingTableCreate().catch(err => {
        console.error(err);
        console.error('Migration stopped at rating table create.');
        error = true;
    });

    console.log(`Ratings table created.`);

})();
