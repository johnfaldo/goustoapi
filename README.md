#Gousto Test API

This project is a RESTful API written in Node's Express framework (with Typescript). 

##Usage

From the project root: 
```bash
npm install
npm run migrate #migrates data
npm run devserver #starts nodemon 
```
The application should now be available at:

**http://localhost:3000**

##Running tests

To run test suites: 

```bash
npm run test
```

The application uses **Jest** and **Supertest** for unit and integration tests. 

**Note:** integration tests run on port 3000, so you **need to stop dev server process** before running test suites.  

##Postman

I've included an export of a Postman collection in the project root which you could use to test end points should you wish.

##Why Express

Great love of Javascript / Express, also was an opportunity to use Typescript with Express - which i'd not used much before. 

I find Express is perfect for small API's and micro services, as it's fast to develop with, secure and easily testable with the same language and testing frameworks used in other parts of the stack. 

##API consumers

The API works the same for all API consumers, it's a public API and can be consumed by any type of client.
