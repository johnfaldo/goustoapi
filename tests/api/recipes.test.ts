let request = null;
let app = null;

import GoustoError from '../../src/services/ErrorService';
import ErrorManager from '../helpers/testHelpers';

let errorManager = new ErrorManager();

const listData: Array<object> = [{id: 1, base: 'something'}, {id: 2, base: 'somethingelse'}];
const showData: object = {id: 1, base: 'something'};
const paginatedData: object = {
    currentPage: 2,
    pageCount: 2,
    size: 2,
    recipes: listData
};
const ratingResponse: object = {message: 'Rating submitted.'};

jest.mock('../../src/services/RecipeService', () => {
    return jest.fn().mockImplementation(() => ({
        list: jest.fn(() => {
            if (errorManager.throwError) {
                return Promise.reject(errorManager.error);
            }
            return Promise.resolve(listData);
        }),
        getById: jest.fn(() => {
            if (errorManager.throwError) {
                return Promise.reject(errorManager.error);
            }
            return Promise.resolve(showData);
        }),
        getByCuisine: jest.fn(() => {
            if (errorManager.throwError) {
                return Promise.reject(errorManager.error);
            }
            return Promise.resolve(paginatedData);
        }),
        update: jest.fn(() => {
            if (errorManager.throwError) {
                return Promise.reject(errorManager.error);
            }
            return Promise.resolve(showData);
        }),
        rateRecipe: jest.fn(() => {
            if (errorManager.throwError) {
                return Promise.reject(errorManager.error);
            }
            return Promise.resolve(ratingResponse);
        })
    }));
});

beforeEach(() => {
    app = require('../../src/app');
    request = require('supertest');
});

afterEach((done) => {
    errorManager.throwError = false;
    errorManager.error = null;
    app.close(() => {
        done();
    });
});

describe('GET 404', () => {

    test('unknown route should give us a 404', async () => {
        const response = await request(app).get('/crazyroute');
        expect(response.statusCode).toEqual(404);
    });

});

describe('GET /', () => {

    test('should return a welcome message', async () => {
        const response = await request(app).get('/');
        expect(response.text).toEqual('Gousto Test API.');
        expect(response.statusCode).toEqual(200);
    });

});

describe('GET /recipes/list', () => {

    test('should return an error response if service rejects promise', async () => {
        errorManager.throwError = true;
        errorManager.error = new GoustoError(errorManager.errorMsg);
        const response = await request(app).get('/recipes/list');
        expect(response.body).toEqual({errors: {message: errorManager.errorMsg}});
        expect(response.statusCode).toEqual(500);
    });

    test('should call controller and return correct response', async () => {
        const response = await request(app).get('/recipes/list').catch(err => {
            throw new Error('Request failed unexpectedly.');
        });
        expect(response.body).toEqual({recipes: listData});
        expect(response.statusCode).toEqual(200);
    });

});

describe('GET /recipes/show', () => {

    test('should return an error response if service rejects promise', async () => {
        errorManager.throwError = true;
        errorManager.error = new GoustoError(errorManager.errorMsg);
        const response = await request(app).get('/recipes/show');
        expect(response.body).toEqual({errors: {message: errorManager.errorMsg}});
        expect(response.statusCode).toEqual(500);
    });

    test('should call controller and return correct response', async () => {
        const response = await request(app).get('/recipes/show').catch(err => {
            throw new Error('Request failed unexpectedly.');
        });
        expect(response.body).toEqual({recipe: showData});
        expect(response.statusCode).toEqual(200);
    });

});

describe('GET /recipes/cuisine/:cuisine/page/:pageNumber', () => {

    test('should return an error response if service rejects promise', async () => {
        errorManager.throwError = true;
        errorManager.error = new GoustoError(errorManager.errorMsg);
        const response = await request(app).get('/recipes/cuisine/cuisine/page/2');
        expect(response.body).toEqual({errors: {message: errorManager.errorMsg}});
        expect(response.statusCode).toEqual(500);
    });

    test('should call controller and return correct response', async () => {
        const response = await request(app).get('/recipes/cuisine/a/page/2').catch(err => {
            throw new Error('Request failed unexpectedly.');
        });
        expect(response.body).toEqual(paginatedData);
        expect(response.statusCode).toEqual(200);
    });

});

describe('PATCH /recipes/update', () => {

    test('should return 500 if service rejects promise', async () => {
        errorManager.throwError = true;
        errorManager.error = new GoustoError(errorManager.errorMsg);
        const response = await request(app).patch('/recipes/1').send({base: 'newbase'});
        expect(response.body).toEqual({errors: {message: errorManager.errorMsg}});
        expect(response.statusCode).toEqual(500);
    });

    test('should call controller and return correct response', async () => {
        const response = await request(app).patch('/recipes/1').send({base: 'newbase'}).catch(err => {
            throw new Error('Request failed unexpectedly.');
        });
        expect(response.body).toEqual({recipe: showData});
        expect(response.statusCode).toEqual(200);
    });

});

describe('PATCH /recipes/create', () => {

    test('should return an error response if service rejects promise', async () => {
        errorManager.throwError = true;
        errorManager.error = new GoustoError(errorManager.errorMsg);
        const response = await request(app).get('/recipes/create');
        expect(response.body).toEqual({errors: {message: errorManager.errorMsg}});
        expect(response.statusCode).toEqual(500);
    });

    test('should call controller and return correct response', async () => {
        const response = await request(app).patch('/recipes/create').send(showData).catch(err => {
            throw new Error('Request failed unexpectedly.');
        });
        expect(response.body).toEqual({recipe: showData});
        expect(response.statusCode).toEqual(200);
    });

});

describe('POST /recipe/:recipeId/rate', () => {

    test('should return an error response if service rejects promise', async () => {
        errorManager.throwError = true;
        errorManager.error = new GoustoError(errorManager.errorMsg);
        const response = await request(app).post('/recipe/1/rate').send({rating: 3});
        expect(response.body).toEqual({errors: {message: errorManager.errorMsg}});
        expect(response.statusCode).toEqual(500);
    });

    test('should call controller and return correct response', async () => {
        const response = await request(app).post('/recipe/1/rate').send({rating: 3}).catch(err => {
            throw new Error('Request failed unexpectedly.');
        });
        expect(response.body).toEqual(ratingResponse);
        expect(response.statusCode).toEqual(200);
    });

});
