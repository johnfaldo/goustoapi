import GoustoError, { IGoustoError } from "../../src/services/ErrorService";

describe('Gousto Error Service', () => {

    it('should set an error message', () => {

        const errorMsg: string = 'Something went wrong!';

        let error: IGoustoError = new GoustoError(errorMsg);

        expect(error.message).toEqual(errorMsg);

    });

    it( 'should default to a 500 error status', () => {

        const expectedStatus = 500;
        const errorMsg: string = 'Something went wrong!';

        let error: IGoustoError = new GoustoError(errorMsg);

        expect(error.status).toEqual(expectedStatus);

    });

    it('should be able to set a status code', () => {

        const expectedStatus = 400;
        const errorMsg: string = 'Something went wrong!';

        let error: IGoustoError = new GoustoError(errorMsg, expectedStatus);

        expect(error.message).toEqual(errorMsg);
        expect(error.status).toEqual(expectedStatus);

    });

});