import DatabaseService, { IDatabaseService } from '../../src/services/DatabaseService';

const mockConnection: object = { connected: true };

jest.mock('sqlite3', () => ({
    verbose: jest.fn(() => ({
      Database: function(file, callback) {
          expect(file).toEqual('./data/gousto.sqlite');
          callback(null);
          return mockConnection;
      }
    }))
}));

describe('Database Service', () => {

    describe('getConnection()', () => {

        it('should construct new sqlite3 Database if one hasn\'t already been instantiated.', () => {

            const databaseService: IDatabaseService = new DatabaseService();
            const connection = databaseService.getConnection();
            expect(connection).toEqual(mockConnection);

        });

    });

});


