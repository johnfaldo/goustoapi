import RecipeService, { IRecipeService } from '../../src/services/RecipeService';
import { IBaseRecipe } from "../../src/interfaces/recipes";
import GoustoError from "../../src/services/ErrorService";

describe('Recipe Service', () => {

    describe('list() - database error', () => {

        const errorMsg: string = 'Something went wrong!';

        const recipeService: IRecipeService = new RecipeService({
            all: (sql, callback) => {
                callback(new GoustoError(errorMsg), null);
            }
        });

        test('should reject if database call returns an error', () => {

            return recipeService.list().then(() => {
                throw new Error('Method resolved promise in error.');
            }).catch(err => {
                expect(err.message).toEqual(errorMsg);
                expect(err.status).toEqual(500);
            });

        });

    });

    describe('list() - happy',  () => {

        const listAllMockData: Array<object> = [{id: 1, name: 'something'}, {id: 2, name: 'something else'}];

        const recipeService: IRecipeService = new RecipeService({
            all: (sql, callback) => {
                callback(null, listAllMockData);
            }
        });

        test('lists all recipes', () => {

            const expectedData: Array<object> = listAllMockData;

            return recipeService.list().then(data => {
                expect(data).toEqual(expectedData);
            });

        });

    });

    describe('getById() - database error', () => {

        const errorMsg: string = 'Something went wrong!';

        const recipeService: IRecipeService = new RecipeService({
            get: (sql, values, callback) => {
                callback(new GoustoError(errorMsg), null);
            }
        });

        test('should reject if database call returns an error', () => {

            return recipeService.getById(1).then(() => {
                throw new Error('Method resolved promise in error.');
            }).catch(err => {
                expect(err.message).toEqual(errorMsg);
                expect(err.status).toEqual(500);
            });

        });

    });

    describe('getById()', () => {

        const recipeId: number = 1;

        const getByIdMockData: IBaseRecipe = {
            created_at: '01/01/01 00:00:00',
            box_type: 'boxtype',
            title: 'title',
            slug: 'slug',
            marketing_description: 'description',
            calories_kcal: 10,
            protein_grams: 20,
            fat_grams: 30,
            carbs_grams: 40,
            recipe_diet_type_id: 'something',
            season: 'all',
            protein_source: 'source',
            preparation_time_minutes: 10,
            shelf_life_days: 5,
            equipment_needed: 'None',
            origin_country: 'Great Britain',
            recipe_cuisine: 'oriental',
            gousto_reference: 1
        };

        const recipeService: IRecipeService = new RecipeService({
            get: (sql, values, callback) => {
                expect(sql).toEqual('SELECT * FROM recipes WHERE id = ?');
                expect(values).toEqual([recipeId]);
                callback(null, getByIdMockData);
            }
        });

        test('should reject with an Error if no ID is passed', () => {

            return recipeService.getById(null).then(() => {
                throw new Error('Method resolved promise in error.');
            }).catch(err => {
                expect(err.status).toEqual(400);
            });

        });

        test('should get a recipe by it\'s ID', () => {

            return recipeService.getById(recipeId).then(data => {
               expect(data).toEqual(getByIdMockData);
            });

        });

    });

    describe('getByCuisine() - missing parameters', () => {

        const recipeService: IRecipeService = new RecipeService({});

        test('should fail if we omit cuisine parameter', () => {

            return recipeService.getByCuisine(null, 1).then(() => {
                throw new Error('Method resolved promise in error.');
            }).catch(err => {
                expect(err.status).toEqual(400);
            });

        });

    });

    describe('getByCuisine() - happy', () => {

        const cuisineAData: Array<object> = [
            {id: 1, cuisine: 'a'},
            {id: 4, cuisine: 'a'},
            {id: 3, cuisine: 'a'}
        ];

        const cuisineBData: Array<object> = [
            {id: 2, cuisine: 'b'},
            {id: 5, cuisine: 'b'}
        ];

        test('cuisine a page 1 should return correct paging data and recipes', () => {

            const cuisine: string = 'a';

            const recipeService: IRecipeService = new RecipeService({
                all: (sql, values, callback) => {
                    expect(values).toEqual([cuisine]);
                    callback(null, cuisineAData);
                }
            });

            const currentPage: number = 1;
            const page1Data: Array<object> = [cuisineAData[0], cuisineAData[1]];

            const expectedResponse: object = {
                currentPage: currentPage,
                pageCount: 2,
                size: 2,
                recipes: page1Data
            };

            return recipeService.getByCuisine(cuisine, currentPage).then(data => {
                expect(data).toEqual(expectedResponse);
            });

        });

        test('cuisine a page 2 should return correct paging data and recipes', () => {

            const cuisine: string = 'a';

            const recipeService: IRecipeService = new RecipeService({
                all: (sql, values, callback) => {
                    expect(values).toEqual([cuisine]);
                    callback(null, cuisineAData);
                }
            });

            const currentPage: number = 2;
            const page2Data: Array<object> = [cuisineAData[2]];

            const expectedResponse: object = {
                currentPage: currentPage,
                pageCount: 2,
                size: 1,
                recipes: page2Data
            };

            return recipeService.getByCuisine(cuisine, currentPage).then(data => {
                expect(data).toEqual(expectedResponse);
            });

        });

        test('cuisine b page 1 should return correct paging data and recipes', () => {

            const cuisine: string = 'b';

            const recipeService: IRecipeService = new RecipeService({
                all: (sql, values, callback) => {
                    expect(values).toEqual([cuisine]);
                    callback(null, cuisineBData);
                }
            });

            const currentPage: number = 1;
            const page1Data: Array<object> = [cuisineBData[0], cuisineBData[1]];

            const expectedResponse: object = {
                currentPage: currentPage,
                pageCount: 1,
                size: 2,
                recipes: page1Data
            };

            return recipeService.getByCuisine(cuisine, currentPage).then(data => {
                expect(data).toEqual(expectedResponse);
            });

        });

    });

    describe('update()', () => {

        describe('database error', () => {

            test('should reject if outer db call returns an error', () => {

                const errorMsg: string = 'Something went wrong!';

                const recipeService: IRecipeService = new RecipeService({
                    run: (sql, values, callback) => {
                        callback(new Error(errorMsg));
                    },
                    get: jest.fn()
                });

                const mockModel: any = {base: 'something', season: 'aseason'};

                return recipeService.update(1, mockModel).then(() => {
                    throw new Error('Method resolved promise in error.');
                }).catch(err => {
                    expect(err.message).toEqual(errorMsg);
                });

            });

        });

        describe('missing parameters', () => {

            const recipeService: IRecipeService = new RecipeService({});

            test('should fail if we omit recipeId', () => {

                return recipeService.update(null, null).then(() => {
                    throw new Error('Method resolved promise in error.');
                }).catch(err => {
                    expect(err.status).toEqual(400);
                });

            });

        });

        describe('invalid model', () => {

            test('should fail if we pass an unknown property in the update model', () => {

                const recipeService: IRecipeService = new RecipeService({});

                const dodgyModel: any = {
                    crazyProperty: 'omg',
                    recipe_cuisine: 'oriental',
                    gousto_reference: 1
                };

                return recipeService.update(1, dodgyModel).then(() => {
                    throw new Error('Method resolved promise in error.');
                }).catch(err => {
                    expect(err.status).toEqual(400);
                });

            });

        });

        describe('happy', () => {

            test('should update correctly', () => {

                const recipeId: number = 1;

                const originalRecipe: IBaseRecipe = {
                    created_at: '01/01/01 00:00:00',
                    box_type: 'boxtype',
                    title: 'title',
                    slug: 'slug',
                    marketing_description: 'description',
                    calories_kcal: 10,
                    protein_grams: 20,
                    fat_grams: 30,
                    carbs_grams: 40,
                    recipe_diet_type_id: 'something',
                    season: 'all',
                    protein_source: 'source',
                    preparation_time_minutes: 10,
                    shelf_life_days: 5,
                    equipment_needed: 'None',
                    origin_country: 'Great Britain',
                    recipe_cuisine: 'oriental',
                    gousto_reference: 1
                };

                const updateModel: any = {
                    title: 'updatedtitle',
                    season: 'updatedseason'
                };

                let expectedResponse: IBaseRecipe = {...originalRecipe};
                expectedResponse.title = updateModel.title;
                expectedResponse.season = updateModel.season;

                const recipeService: IRecipeService = new RecipeService({
                    run: (sql, values, callback) => {
                        expect(values).toEqual([...Object.values(updateModel), recipeId]);
                        callback(null)
                    },
                    get: (sql, callback) => {
                        callback(null, expectedResponse);
                    }
                });

                return recipeService.update(recipeId, updateModel).then(data => {
                    expect(data).toEqual(expectedResponse);
                });

            });

        });

    });

    describe('create()', () => {

        describe('database error', () => {

            test('should reject db returns an error', () => {

                const errorMsg: string = 'Something went wrong!';

                const recipeService: IRecipeService = new RecipeService({
                    run: (sql, values, callback) => {
                        callback(new Error(errorMsg));
                    },
                    get: jest.fn()
                });

                const mockModel: any = {base: 'something', season: 'aseason'};

                return recipeService.create(mockModel).then(() => {
                    throw new Error('Method resolved promise in error.');
                }).catch(err => {
                    expect(err.message).toEqual(errorMsg);
                });

            });

        });

        describe('invalid model', () => {

            test('should fail if we pass an unknown property in the update model', () => {

                const recipeService: IRecipeService = new RecipeService({});

                const dodgyModel: any = {
                    crazyProperty: 'omg',
                    recipe_cuisine: 'oriental',
                    gousto_reference: 1
                };

                return recipeService.create(dodgyModel).then(() => {
                    throw new Error('Method resolved promise in error.');
                }).catch(err => {
                    expect(err.status).toEqual(400);
                });

            });

        });

        describe('happy', () => {

            test('should create correctly', () => {

                const newRecipe: any = {
                    created_at: '01/01/01 00:00:00',
                    box_type: 'boxtype',
                    title: 'title',
                    slug: 'slug',
                    marketing_description: 'description',
                    calories_kcal: 10,
                    protein_grams: 20,
                    fat_grams: 30,
                    carbs_grams: 40,
                    recipe_diet_type_id: 'something',
                    season: 'all',
                    protein_source: 'source',
                    preparation_time_minutes: 10,
                    shelf_life_days: 5,
                    equipment_needed: 'None',
                    origin_country: 'Great Britain',
                    recipe_cuisine: 'oriental',
                    gousto_reference: 1
                };

                const recipeService = new RecipeService({
                    run: (sql, values, callback) => {
                        expect(values).toEqual(Object.values(newRecipe));
                        callback.bind({lastID: 1})(null);
                    },
                    get: (sql, callback) => {
                        callback(null, newRecipe);
                    }
                });

                return recipeService.create(newRecipe).then(data => {
                    expect(data).toEqual(newRecipe);
                });

            });

        });

    });

    describe('rateRecipe()', () => {

        describe('database error', () => {

            const errorMsg: string = 'Something went wrong!';

            const recipeService: IRecipeService = new RecipeService({
                run: (sql, values, callback) => {
                    callback(new Error(errorMsg), null);
                }
            });

            test('should reject if database call returns an error', () => {

                return recipeService.rateRecipe(1, 1).then(() => {
                    throw new Error('Method resolved promise in error.');
                }).catch(err => {
                    expect(err.message).toEqual(errorMsg);
                });

            });

        });

        describe('missing parameters', () => {

            const recipeService: IRecipeService = new RecipeService({});

            test('should fail if we omit recipeID parameter', () => {

                return recipeService.rateRecipe(null, 1).then(() => {
                    throw new Error('Method resolved promise in error.');
                }).catch(err => {
                    expect(err.status).toEqual(400);
                });

            });

        });

        describe('happy', () => {

            test('should rate a recipe', () => {

                const recipeId: number = 1;
                const rating: number = 5;

                const recipeService: IRecipeService = new RecipeService({
                    run: (sql, values, callback) => {
                        expect(sql).toEqual('INSERT INTO ratings (recipe_id, rating, created_at) VALUES (?, ?, CURRENT_TIMESTAMP)');
                        expect(values).toEqual([recipeId, rating]);
                        callback(null);
                    }
                });

                return recipeService.rateRecipe(recipeId, rating).then((data) => {
                    expect(data).toEqual(undefined);
                });

            });

        });

    });

});


