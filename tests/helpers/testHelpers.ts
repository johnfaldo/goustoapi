import {IGoustoError} from "../../src/services/ErrorService";

export interface IErrorManager {
    throwError: boolean;
    error: IGoustoError;
    errorMsg: string;
}

export default class ErrorManager implements IErrorManager {

    _throwError: boolean = false;
    _error: IGoustoError = null;
    _errorMsg: string = 'Something went wrong!'

    set error(error: IGoustoError) {
        this._error = error;
    }

    get error(): IGoustoError {
        return this._error;
    }

    set throwError(value: boolean) {
        this._throwError = value;
    }

    get throwError(): boolean {
        return this._throwError;
    }

    set errorMsg(errorMsg: string) {
        this._errorMsg = errorMsg;
    }

    get errorMsg(): string {
        return this._errorMsg;
    }

}
